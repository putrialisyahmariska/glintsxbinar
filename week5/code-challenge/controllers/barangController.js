const { barang, pemasok } = require('../models')

// make BarangController class
class BarangController {

  // Get all data function
  async getAll(req, res) {
    // Find all data collection data
    barang.find({}).then(result => {
      res.json({
        status: 'success',
        data: result
      })
    })
  }
  // Get one data
  async getOne(req, res) {
    // Find one
    barang.findOne({
      _id: req.params.id
    }).then(result => {
      res.json({
        status: 'success',
        data: result
      })
    })
  }
  async create(req, res) {
    const data = await Promise.all([
      pemasok.findOne({
        _id: req.body.id_pemasok
      })
    ])

    barang.create({
      pemasok: data[0],
      nama: req.body.nama,
      harga: req.body.harga
    }).then((result) => {
      res.json({
        status: 'success',
        data: result
      })
    })
  }
  async update(req, res) {
    const data = await Promise.all([
      pemasok.findOne({
        _id: req.body.id_pemasok
      })
    ])
    barang.findOneAndUpdate({
      _id: req.params.id
    }, {
      "pemasok": data[0],
      "nama": req.body.nama,
      "harga": req.body.harga
    }).then(result => {
      res.json({
        status: "success",
        data: result
      })
    })


  }
  async delete(req, res) {
    barang.delete({
      _id: req.params.id
    }).then(() => {
      res.json({
        status: "success",
        data: null
      })
    })
  }
}
module.exports = new BarangController();