const express = require('express') // Import express
const router = express.Router() // Make router from app
const PelangganController = require('../controllers/pelangganController.js') 
const pelangganValidator = require('../middlewares/validators/pelangganValidator.js') 

router.get('/', PelangganController.getAll) 
router.get('/:id', pelangganValidator.getOne, PelangganController.getOne) 
router.post('/create', pelangganValidator.create, PelangganController.create) 
router.put('/update/:id', pelangganValidator.update, PelangganController.update) 
router.delete('/delete/:id', pelangganValidator.delete, PelangganController.delete) 

module.exports = router; // Export router