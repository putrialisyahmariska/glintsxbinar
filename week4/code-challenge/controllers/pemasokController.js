const { Transaksi, Barang, Pelanggan, Pemasok } = require("../models");
const {
	check,
	validationResult,
	matchedData,
	sanitize,
} = require("express-validator"); //form validation & sanitize form params

class PemasokController {
	constructor() {
		// Barang.hasMany(Transaksi, {
		// 	foreignKey: "id_barang",
		// });
		// Pelanggan.hasMany(Transaksi, {
		// 	foreignKey: "id_pelanggan",
		// });
		// Transaksi.belongsTo(Barang, {
		// 	foreignKey: "id_barang",
		// });
		// Transaksi.belongsTo(Pelanggan, {
		// 	foreignKey: "id_barang",
		// });
	}

	// Get All data from transaksi
	async getAll(req, res) {
		Pemasok.findAll({
			// find all data of Transaksi table
			attributes: ["id", "nama", ["createdAt", "waktu"]], // just these attributes that showed
		}).then((transaksi) => {
			res.json(transaksi); // Send response JSON and get all of Transaksi table
		});
	}

	// Get One data from transaksi
	async getOne(req, res) {
		Pemasok.findOne({
			// find one data of Transaksi table
			where: {
				id: req.params.id, // where id of Transaksi table is equal to req.params.id
			},
			attributes: ["id", "nama", ["createdAt", "waktu"]], // just these attributes that showed
		}).then((transaksi) => {
			res.json(transaksi); // Send response JSON and get one of Transaksi table depend on req.params.id
		});
	}

	// Create Transaksi data
	async create(req, res) {
		Pemasok.create({
			nama: req.body.nama,
		}).then((newPemasok) => {
			res.json({
				status: "success",
				message: "Pemasok added",
				data: newPemasok,
			});
		});
	}

	// Update Transaksi data
	async update(req, res) {
		Pemasok.update(
			{
				nama: req.body.nama,
			},
			{
				where: { id: req.params.id },
			}
		)
			.then((affectedRow) => {
				return Pemasok.findOne({ where: { id: req.params.id } });
			})
			.then((b) => {
				res.json({
					status: "success",
					message: "Pelanggan updated",
					data: b,
				});
			});
	}
	// Soft delete Transaksi data
	async delete(req, res) {
		Pemasok.destroy({
			// Delete data from Transaksi table
			where: {
				id: req.params.id, // Where id of Transaksi table is equal to req.params.id
			},
		})
			.then((affectedRow) => {
				// If delete success, it will return this JSON
				if (affectedRow) {
					return {
						status: "success",
						message: "Pemasok deleted",
						data: null,
					};
				}

				// If failed, it will return this JSON
				return {
					status: "error",
					message: "Failed",
					data: null,
				};
			})
			.then((r) => {
				res.json(r); // Send response JSON depends on failed or success
			});
	}
}

module.exports = new PemasokController();
