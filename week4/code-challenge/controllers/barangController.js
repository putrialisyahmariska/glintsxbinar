const { Transaksi, Barang, Pelanggan, Pemasok } = require("../models");
const {
	check,
	validationResult,
	matchedData,
	sanitize,
} = require("express-validator"); //form validation & sanitize form params

class BarangController {
	constructor() {
		Barang.hasMany(Transaksi, {
			foreignKey: "id_barang",
		});
		Pelanggan.hasMany(Transaksi, {
			foreignKey: "id_pelanggan",
		});
		Transaksi.belongsTo(Barang, {
			foreignKey: "id_barang",
		});
		Transaksi.belongsTo(Pelanggan, {
			foreignKey: "id_barang",
		});
		Barang.belongsTo(Pemasok, {
			foreignKey: "id_pemasok",
		});
	}

	// Get All data from transaksi
	async getAll(req, res) {
		Barang.findAll({
			// find all data of Transaksi table
			attributes: [
				"id",
				"nama",
				"harga",
				"id_pemasok",
				"image",
				["createdAt", "waktu"],
			], // just these attributes that showed
			include: [
				{
					model: Pemasok,
					attributes: ["nama"], // just this attrubute from Barang that showed
				},
			],
		}).then((transaksi) => {
			res.json(transaksi); // Send response JSON and get all of Transaksi table
		});
	}

	// Get One data from transaksi
	async getOne(req, res) {
		Barang.findOne({
			// find one data of Transaksi table
			where: {
				id: req.params.id, // where id of Transaksi table is equal to req.params.id
			},
			attributes: [
				"id",
				"nama",
				"harga",
				"id_pemasok",
				"image",
				["createdAt", "waktu"],
			], // just these attributes that showed
			include: [
				{
					model: Pemasok,
					attributes: ["nama"], // just this attrubute from Barang that showed
				},
			],
		}).then((transaksi) => {
			res.json(transaksi); // Send response JSON and get one of Transaksi table depend on req.params.id
		});
	}

	// Create Transaksi data
	async create(req, res) {
		Barang.create({
			nama: req.body.nama,
			harga: req.body.harga,
			id_pemasok: req.body.id_pemasok,
			image: req.file === undefined ? "" : req.file.filename,
		}).then((newBarang) => {
			res.json({
				status: "success",
				message: "Barang added",
				data: newBarang,
			});
		});
	}

	// Update Transaksi data
	async update(req, res) {
		if (req.file !== undefined) {
			var img = req.file.filename;
		}
		Barang.update(
			{
				nama: req.body.nama,
				harga: req.body.harga,
				id_pemasok: req.body.id_pemasok,
				image: img,
			},
			{
				where: { id: req.params.id },
			}
		)
			.then((affectedRow) => {
				return Barang.findOne({ where: { id: req.params.id } });
			})
			.then((b) => {
				res.json({
					status: "success",
					message: "Barang updated",
					data: b,
				});
			});
	}
	// Soft delete Transaksi data
	async delete(req, res) {
		Barang.destroy({
			// Delete data from Transaksi table
			where: {
				id: req.params.id, // Where id of Transaksi table is equal to req.params.id
			},
		})
			.then((affectedRow) => {
				// If delete success, it will return this JSON
				if (affectedRow) {
					return {
						status: "success",
						message: "Barang deleted",
						data: null,
					};
				}

				// If failed, it will return this JSON
				return {
					status: "error",
					message: "Failed",
					data: null,
				};
			})
			.then((r) => {
				res.json(r); // Send response JSON depends on failed or success
			});
	}
}

module.exports = new BarangController();
