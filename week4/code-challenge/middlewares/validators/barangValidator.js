const {
	check,
	validationResult,
	matchedData,
	sanitize,
} = require("express-validator"); //form validation & sanitize form params
const multer = require("multer"); //multipar form-data
const path = require("path");
const crypto = require("crypto");
const { Transaksi, Barang, Pelanggan, Pemasok } = require("../../models"); // Import models

// //Set body parser for HTTP post operation
// app.use(bodyParser.json()); // support json encoded bodies
// app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

//set static assets to public directory
// app.use(express.static("public"));
const uploadDir = "/img/";
const storage = multer.diskStorage({
	destination: "./public" + uploadDir,
	filename: function (req, file, cb) {
		crypto.pseudoRandomBytes(16, function (err, raw) {
			if (err) return cb(err);

			cb(null, raw.toString("hex") + path.extname(file.originalname));
		});
	},
});

const upload = multer({ storage: storage, dest: uploadDir });
module.exports = {
	create: [
		upload.single("image"),

		check("nama").isLength({ min: 5, max: 255 }),
		check("id_pemasok").custom((value) => {
			return Pemasok.findOne({ where: { id: value } }).then((b) => {
				if (!b) {
					throw new Error("ID Pemasok Not Found");
				}
			});
		}),
		check("harga").isNumeric(),
		(req, res, next) => {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return res.status(422).json({
					errors: errors.mapped(),
				});
			}
			next();
		},
		//Set form validation rule
		// check("isbn")
		// 	.isLength({ min: 5 })
		// 	.isNumeric()
		// 	.custom((value) => {
		// 		return book.findOne({ where: { isbn: value } }).then((b) => {
		// 			if (b) {
		// 				throw new Error("ISBN already in use");
		// 			}
		// 		});
		// 	}),
	],
	update: [
		upload.single("image"),
		check("id")
			.isNumeric()
			.custom((value) => {
				return Barang.findOne({ where: { id: value } }).then((b) => {
					if (!b) {
						throw new Error("Barang ID not found");
					}
				});
			}),
		check("nama").isLength({ min: 5, max: 255 }),
		(req, res, next) => {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return res.status(422).json({
					errors: errors.mapped(),
				});
			}
			next();
		},
	],
};
