const fs = require('fs')

const readFile = options => file => new Promise((resolve, reject) => {
    fs.readFile(file, options, (err, content) => {
      if (err) return reject(err)
      return resolve(content)
    })
  })
  
  const writeFile = (file, content) => new Promise((resolve, reject) => {
    fs.writeFile(file, content, err => {
      if (err) return reject(err)
      return resolve()
    })
  })
  
  const
    read = readFile('utf-8')
  
  
  // Promise.race([read('contents/content1.txt'), read('contents/content2.txt'), read('contents/content3.txt'), read('contents/content4.txt'), read('contents/content5.txt'), read('contents/content6.txt'), read('contents/content7.txt'), read('contents/content8.txt'), read('contents/content9.txt'), read('contents/content10.txt')])
  //   .then((value) => {
  //     console.log(value);
  //   })
    Promise.race([read('content1.txt'), read('content2.txt'), read('content3.txt'), read('content4.txt'), read('content5.txt'), read('content6.txt'), read('content7.txt'), read('content8.txt'), read('content9.txt'), read('content10.txt')])
    .then((value) => {
      console.log(value);
    })
    .catch(error => {
      console.log(error);
    })

  