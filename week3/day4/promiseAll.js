const fs = require('fs')

const readFile = options => file => new Promise((resolve, reject) => {
  fs.readFile(file, options, (err, content) => 
  {if (err) return reject(err)
    return resolve(content)}
    )
})

const writeFile = (file, content) => new Promise((resolve, reject) => {
  fs.writeFile(file, content, err => {
    if (err) return reject(err)
    return resolve()
  })
})



const
  read = readFile('utf-8')

async function mergedContent() {
  try {
    
    const result = await Promise.all([
      read('content1.txt'),
      read('content2.txt'),
      read('content3.txt'),
      read('content4.txt'),
      read('content5.txt'),
      read('content6.txt'),
      read('content7.txt'),
      read('content8.txt'),
      read('content9.txt'),
      read('content10.txt')
    ])
    
    await writeFile('contents.txt', result.join(' '))
  } catch (e) {
    throw e
  }

  
  return read('contents.txt')
}

mergedContent()
  .then(result => {
    console.log(result) 
  }).catch(err => {
    console.log('Error to read/write file, error: ', err)
  }).finally(() => {
    console.log('Allahuakbar');
  })

