const fs = require('fs')


const readFile = options => file => new Promise((resolve, reject) => {
  fs.readFile(file, options, (err, content) => {
    if (err) return reject(err)
    return resolve(content)
  })
})

const writeFile = (file, content) => new Promise((resolve, reject) => {
  fs.writeFile(file, content, err => {
    if (err) return reject(err)
    return resolve()
  })
})

const
  read = readFile('utf-8'),
  files = ['content1.txt', 'content2.txt', 'content3.txt', 'content4.txt', 'content5.txt', 'content6.txt', 'content7.txt', 'content8.txt', 'content9.txt', 'content10.txt']


Promise.allSettled(files.map(file => read(`${file}`)))
  .then(results => {

    console.log(results)
  })
