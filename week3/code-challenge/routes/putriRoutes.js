
const express = require('express') 
const router = express.Router() 
const putriController = require('../controllers/putriController.js') 

router.get('/', putriController.putri)

module.exports = router; // 
