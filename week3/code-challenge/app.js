
const express = require('express') 
const app = express()
const putriRoutes = require('./routes/putriRoutes.js') 

app.use(express.static('public')); 


app.use('/putri', putriRoutes)

app.listen(3000) 