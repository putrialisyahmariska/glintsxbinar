const data = require('./lib/arrayFactory.js');
const test = require('./lib/test.js');

/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter(i => typeof i === 'number');
}

// Should return array
function sortAscending(data) {
    var data = clean(data)
    for (let i = 0; i < data.length; i++) {
      for (let j = 0; j < data.length - i; j++) {
        if (data[j-1] > data[j]) {
          let store = data[j-1]
          data[j-1] = data[j]
          data[j] = store
        }
      }
    }
    return data;
  }
  
  // Should return array
  function sortDecending(data) {
    var data = clean(data)
    for (let i = 0; i < data.length; i++) {
      for (let j = 0; j < data.length - i; j++) {
        if (data[j-1] < data[j]) {
          let store = data[j-1]
          data[j-1] = data[j]
          data[j] = store
        }
      }
    }
    return data;
  }

// DON'T CHANGE
test(sortAscending, sortDecending, data);
