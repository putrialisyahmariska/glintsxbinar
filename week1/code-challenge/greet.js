const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

function greet(name, address, birthday) {
  // Insert your code here!
  const currentDate = new Date ();
  const currentYear = currentDate.getFullYear();
  console.log("Hello, " + name + "! Looks like you're " + (currentYear - birthday) + " years old, and you lived in " + address + "!");
}
// Hello, Fikri! Looks like you're 20 years old, and you lived in Surakarta!

// DON'T CHANGE
console.log("Goverment Registry\n")
// GET User's Name
rl.question("What is your name? ",  name => { 
  // GET User's Address
  rl.question("Which city do you live? ", address => {
    // GET User's Birthday
    rl.question("When was your birthday year? ", birthday => {
      greet(name, address, birthday)

      rl.close()
    })
  })
})

rl.on("close", () => {
  process.exit()
})
